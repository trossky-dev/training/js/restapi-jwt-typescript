# Curso de intro

La idea principal de este proyecto es aplicar los conceptos básicos para trabajar con proyectos de javascript.

Algo que destaca este proyecto es que se ha desarrollado en NODEJS usnado TYPESCRIPT como lengueje.

## Continuar el cuso AQUI

La referencia principal del proyecto, es basado en un curso de [Fast Code](https://youtu.be/qVUr4YC6ZXA)

## Descripción de principales Librerias 

````js
"dependencies": {
    "express": "^4.17.1",
    "mongoose": "^5.11.8",
    "morgan": "^1.10.0",  // permite ver el flujo de peticiones http de la api a nuestro backend
    "typescript": "^4.1.3"
  },
  "devDependencies": {
    "@types/express": "^4.17.9",
    "@types/mongoose": "^5.10.3",
    "@types/morgan": "^1.9.2",
    "concurrently": "^5.3.0", // permite ejecutar en un mismo comando de script mas de un comando de forma concurrente, Ej. "dev": " concurrently \"tsc -w \" \"nodemon  dist/index.js\" ",
    "nodemon": "^2.0.6" // Es un Watch el cual monitorea los cambios en los archivo y compila automaticamente el proyecto.
  }

```