import mongoose from "mongoose";

mongoose.connect('mongodb://192.168.1.5/test',{
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
})
  .then(db => console.log("Db is connected!"))
  .catch(error => console.log(error))