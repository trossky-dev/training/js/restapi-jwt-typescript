import {Request, Response} from 'express';
import User,{IUser} from "../models/User";
import jwt from 'jsonwebtoken';

export const signup = async (req:Request, res:Response) =>{

  const {username, email, password}= req.body

  // saved new user
  const newUser:IUser = new User({
    username:username,
    email:email,
    password:password
  })
  newUser.password = await newUser.encryptPassword(newUser.password) 
  const savedUser:IUser = await newUser.save();

  // token create
  const token:string= jwt.sign({_id:savedUser._id},process.env.SECRET_KEY_JWT|| 'tokentest')
  console.log(savedUser);
  res.header('auth-token',token).json(savedUser)

}

export const signin = async (req:Request, res:Response)=>{
  

  const { email, password}= req.body
  const user = await User.findOne({email:email});

  if(!user){
    return res.status(400).json('Email o password is wrong')
  }

  const correctPassword= await user.validatePassword(password);

  if(!correctPassword){
    return res.status(400).json('Invalid password ')
  }


  // token create
  const token:string= jwt.sign({_id:user._id},process.env.SECRET_KEY_JWT|| 'tokentest',
  {
    expiresIn:60*60*24
  })

  res.header('auth-token',token).json(user)

}
export const profile = async (req:Request, res:Response)=>{
  // reponde el perfil del usuario, pero sin la contraseña
  const user= await User.findById(req.userId, {password:0});
  
  if(!user) return res.status(404).json("No user found");
  res.json(user);

}

export const health = (req:Request, res:Response)=>{

  res.send("Health Ok!")

}

