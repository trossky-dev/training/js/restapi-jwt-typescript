import {Request,Response,NextFunction} from 'express';

import jwt from 'jsonwebtoken'

interface IPayload {
    _id: string,
    iat:number,
    exp:number
}

//Define a middleware
export const TokenValidations =(req:Request, res:Response,next:NextFunction)=>{
    const token = req.header('auth-token');
    if(!token) return res.status(401).json('Access denied');

    const playload = jwt.verify(token,process.env.SECRET_KEY_JWT || 'tokentest') as IPayload
    req.userId =playload._id;
    
    console.log(playload);

    next();

}