import express, {Application} from "express";
import authRoute from "./routes/auth";

import morgan from "morgan";

const app: Application = express();

// settings
app.set("port", 5000);
// Middlewares 

app.use(morgan('dev'))
app.use(express.json());


//  Routes
app.use("/api/auth",authRoute);


export default app;