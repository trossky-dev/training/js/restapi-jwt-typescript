import { Router } from 'express';
import { TokenValidations } from '../libs/verifyToken';
import { signup, signin, profile, health } from '../controllers/auth.controller'

const router:Router =Router();

router.post('/signup', signup);
router.post('/signin', signin);
router.get('/profile', TokenValidations, profile);

router.get('/health', health);


export default router;