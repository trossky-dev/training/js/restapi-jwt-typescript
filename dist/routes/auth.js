"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const verifyToken_1 = require("../libs/verifyToken");
const auth_controller_1 = require("../controllers/auth.controller");
const router = express_1.Router();
router.post('/signup', auth_controller_1.signup);
router.post('/signin', auth_controller_1.signin);
router.get('/profile', verifyToken_1.TokenValidations, auth_controller_1.profile);
router.get('/health', auth_controller_1.health);
exports.default = router;
//# sourceMappingURL=auth.js.map